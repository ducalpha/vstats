#! /usr/bin/env python
from numpy import *

filename = "vstats.txt"
import Gnuplot, Gnuplot.funcutils
def demo():
	label = ""
	f = open(filename)
	while 1:
		line = f.readline()
		if not line:
			break
		if line.startswith("#"):
			if not line.startswith("#Time"):
				print line
				label += line.strip()
	f.close()

	g = Gnuplot.Gnuplot(debug=1)
	g.title('Video statistics ' + label)
	g('set style data linespoints') # give gnuplot an arbitrary command
	g.xlabel('Time (second)')
	g.ylabel('Decoded data (Mbps)')
	g('plot "' + filename + '" using 1:2 title \'Decoded data\'')
	raw_input('Press any key to continue...\n')

# when executed, just run demo():
if __name__ == '__main__':
    demo()

